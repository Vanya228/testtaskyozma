// import axios from "axios"; // can be used for requests if needed

const URL_SERVER_A = "serv_a_link/";
const URL_SERVER_B = "serv_b_link/";

const testUsername = "user";
const testPassword = "123";

//example of sign in function
export const userSignIn = async (username, password) => {
  try {
    const url = URL_SERVER_A + "auth/login";

    // example of getting data from user sign in endpoint
    // const headers = {
    //   api_key: "c0082183-35d6-4d39-ac7b-819c546c3b73",
    //   secret_key: "ecf3ff4a-78d8-4362-9824-3fd272 bb3499",
    // };
    // fetch(url, { headers })
    //   .then((response) => response.json())
    //   .then((data) => {
    //     console.log("Success:", data);
    //   });
    return username == testUsername && password == testPassword ? 200 : 404;
  } catch (error) {
    console.log(`userSignIn error: `, error);
    return {};
  }
};

//example of getting user data
export const getProfileInfo = async (token) => {
  try {
    const url = URL_SERVER_B + "auth/user";

    //example of getting data from get profile info endpoint
    // let response = await fetch(url, {
    //   headers: {
    //     Authorization: `Bearer ${token}`, // token for example
    //   },
    // });

    let obj = {
      name: "Cat user",
      age: 28,
      location: "Dubai",
    };

    return obj;
  } catch (error) {
    console.log(`getProfileInfo error: `, error);
    return {};
  }
};
