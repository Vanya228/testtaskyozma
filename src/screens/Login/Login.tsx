import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { scale } from '../../common/common'
import { colors } from '../../common/colors'
import { useNavigation } from '@react-navigation/native';
import { userSignIn } from '../../api/index'

export const Login = () => {

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [loginError, setLoginError] = useState(false)

    const navigation = useNavigation()

    const loginAction = async () => {
        const signIn = await userSignIn(username, password)
        if (signIn == 200) {
            setLoginError(false)
            navigation.navigate('Home')
        } else {
            setLoginError(true)
        }
    }

    const onChangeUsername = (query: any) => {
        setUsername(query)
    }
    const onChangePassword = (query: any) => {
        setPassword(query)
    }


    return (
        <View style={styles.container}>
            <Text style={styles.loginText}>My cat</Text>
            <Text style={[styles.loginText, { marginTop: scale(30), fontSize: scale(30) }]}>Already have an account?</Text>

            <View style={{ marginTop: scale(50) }}>
                <TextInput
                    style={[styles.textInput, {borderColor: loginError ? 'red' : 'lightgray'}]}
                    placeholder={'Email'}
                    onChangeText={onChangeUsername}
                />
                <TextInput
                    style={[styles.textInput, {borderColor: loginError ? 'red' : 'lightgray'}]}
                    placeholder={'Password'}
                    onChangeText={onChangePassword}
                    secureTextEntry={true}
                />
            </View>
            
            <TouchableOpacity style={styles.forgotPassword} onPress={() => { }}>
                <Text style={{ color: 'black', textDecorationLine: 'underline' }}>Forgot password</Text>
            </TouchableOpacity>

            {loginError ? <Text style={styles.errorText}>Invalid username or password</Text> : null}


            <TouchableOpacity style={styles.signInBtn} onPress={loginAction}>
                <Text style={styles.signInBtnText}>Sign in</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    loginText: {
        color: 'gray',
        fontSize: scale(50),
        alignSelf: 'center',
        marginTop: scale(200),
        textAlign: 'center'
    },
    textInput: {
        alignSelf: 'center',
        width: '80%',
        height: scale(80),
        backgroundColor: 'white',
        borderWidth: scale(3),
        borderRadius: scale(20),
        marginTop: scale(20),
        paddingLeft: scale(30)
    },
    regBtn: {
        width: scale(250),
        height: scale(66),
        borderRadius: scale(30),
        backgroundColor: colors.mainOrange,
        alignSelf: 'center',
        marginTop: scale(30)
    },
    regBtnText: {
        color: 'white',
        fontSize: scale(30),
        alignSelf: 'center',
        marginTop: scale(10)
    },
    signInBtn: {
        width: scale(250),
        height: scale(66),
        borderRadius: scale(30),
        backgroundColor: colors.mainOrange,
        alignSelf: 'center',
        marginTop: scale(40)
    },
    signInBtnText: {
        color: 'white',
        fontSize: scale(30),
        alignSelf: 'center',
        marginTop: scale(10)
    },
    forgotPassword: {
        alignSelf: 'center',
        marginTop: scale(40),

    },
    errorText: {
        color: 'red',
        alignSelf: 'center',
        fontSize: scale(35)
    }
})