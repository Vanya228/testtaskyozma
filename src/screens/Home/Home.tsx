import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, ScrollView } from 'react-native'
import React, { useEffect, useState } from 'react'
import { scale } from '../../common/common'
import { useNavigation } from '@react-navigation/native';
import { CarouselItem } from './Components/CarouselItem';
import { cards } from '../../common/tempData'
import { bannerItems } from '../../common/tempData';
import { cardSwipeData } from '../../common/tempData';
import { BannerItem } from './Components/BannerItem';
import {  useSelector } from 'react-redux';
import { useAction } from '../../utils/useAction'
import { addUserDataAction } from '../../store/actions/login'
import { userSelector } from '../../store/selectors/login';
import {getProfileInfo} from '../../api'

export const Home = () => {

  const navigation = useNavigation()
  const [cardsOneData, setCardsOneData] = useState([])
  const [cardSwipeDataTwo, setCardSwipeDataTwo] = useState([])
  const [bannerItemsData, setBannerItemsData] = useState([])
  const [userName, setUserName] = useState('Unknown')
  const addUserData = useAction(addUserDataAction)
  const userData = useSelector(userSelector)

  const getUserData = async () => {
    const data = await getProfileInfo('token')
    addUserData({
      name: data.name,
      age: data.age,
      location: data.location
    })
  }
  
  useEffect(()=>{
    getUserData()
  }, [])

  useEffect(() => {
    setUserName(userData.user.name)
  }, [userData])
  
  useEffect(() => {
    setCardsOneData(cards)
  }, [cards])

  useEffect(() => {
    setCardSwipeDataTwo(cardSwipeData)
  }, [cardSwipeData])

  useEffect(() => {
    setBannerItemsData(bannerItems)
  }, [bannerItems])

  return (
    <View style={styles.container}>
      <View style={styles.pageHeader}>

        <Text style={styles.userNameText}>{userName}</Text>

        <TouchableOpacity onPress={() => { navigation.navigate('Profile') }}>
          <Image style={styles.userIco} source={require('../../assets/temp/tempImages/userAvatarTemp.png')} />
        </TouchableOpacity>

      </View>

      <ScrollView nestedScrollEnabled style={styles.pageContent} showsVerticalScrollIndicator={false}>

        <Text style={{ fontSize: 20 }}>Cards</Text>

        <View style={styles.cardsWrapper}>

          <FlatList
            data={cardsOneData}
            renderItem={(item) => {
              return <CarouselItem data={item.item} />
            }}
            keyExtractor={(item) => item.id}
            showsVerticalScrollIndicator={false}
          />

        </View>

        <View style={styles.cardSwipeDataContainer}>

          <Text style={{ fontSize: 20 }}>Vertical Cards</Text>

          <FlatList
            data={cardSwipeDataTwo}
            renderItem={(item) => {
              return (
                <View style={styles.cardSwipeDataItemContainer}>
                  <Image style={styles.cardSwipeDataImage} source={item.item.img} resizeMode="cover" />
                  <Text style={styles.cardSwipeDataText}>{item.item.text}</Text>
                </View>
              )
            }}
            keyExtractor={(item) => item.id}
            showsVerticalScrollIndicator={false}
          />

        </View>

      </ScrollView>

      <View style={styles.bannerWrapper}>
        <FlatList
          data={bannerItemsData}
          renderItem={(item) => {
            return <BannerItem data={item.item} />
          }}
          horizontal={true}
          keyExtractor={(item) => {
            item.id.toString()
          }
          }
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          bounced={false}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  pageHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: scale(100),
    width: '100%'
  },
  userNameText: {
    marginTop: scale(30),
    marginLeft: scale(50),
    fontSize: scale(50)
  },
  userIco: {
    width: scale(70),
    height: scale(70),
    marginTop: scale(30),
    marginRight: scale(50)
  },
  pageContent: {
    marginTop: scale(30),
    alignSelf: 'center',
    width: '90%',
    height: scale(200),
    marginBottom: scale(230)
  },
  cardsWrapper: {
    width: '100%',
    height: scale(455)
  },
  bannerWrapper: {
    width: '100%',
    height: scale(250),
    backgroundColor: 'white',
    marginTop: scale(30),
    position: 'absolute',
    bottom: 0
  },
  cardSwipeDataContainer: {
    marginTop: scale(20),
    height: scale(300)
  },
  cardSwipeDataItemContainer: {
    backgroundColor: 'white',
    width: '100%',
    height: scale(300)
  },
  cardSwipeDataImage: {
    position: 'absolute',
    height: scale(300)
  },
  cardSwipeDataText: {
    color: 'white',
    fontSize: scale(40),
    width: scale(400),
    alignSelf: 'center',
    textAlign: 'center',
    marginTop: scale(70)
  }
})