import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import Icon from "react-native-vector-icons/MaterialIcons";
import React from 'react'
import { scale } from '../../../common/common'
import { useNavigation } from '@react-navigation/native';

export const RenderItem = (props) => {

    const {
        action,
        iconName,
        image,
        text
    } = props.item

    const navigation = useNavigation()

    return (
        <View style={styles.renderItem}>
            
            <Image source={image} resizeMode="cover" style={styles.bg} />

            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.text}>{text}</Text>
                <Icon name={iconName} size={15} style={styles.icon} />
            </View>

            <TouchableOpacity style={styles.cardBtn} onPress={()=>{
                console.log('action', action)
                if(action === "navToLogin") navigation.replace('Login')
                else if(action === "navToLogin") navigation.navigate('Settings')
            }}>
                <Text style={styles.btnText}>Button</Text>
            </TouchableOpacity>

        </View>
    )
}

const styles = StyleSheet.create({
    renderItem: {
        width: scale(645),
        height: '100%'
    },
    bg: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    text: {
        color: 'black',
        marginLeft: scale(15),
        marginTop: scale(15)
    },
    icon: {
        marginTop: scale(16),
        marginLeft: scale(5)
    },
    cardBtn: {
        width: scale(150),
        height: scale(50),
        borderRadius: scale(30),
        borderWidth: scale(2),
        borderColor: 'gray',
        marginTop: scale(15),
        marginLeft: scale(15),
        backgroundColor: 'lightgray'
    },
    btnText: {
        alignSelf: 'center',
        marginTop: scale(3)
    }
})