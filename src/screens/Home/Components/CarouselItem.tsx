import { StyleSheet, View, FlatList } from 'react-native'
import React from 'react'
import { scale } from '../../../common/common'
import { RenderItem } from './RenderItem'

export const CarouselItem = (props) => {

  const CarouselItems = props.data

  return (
    <View style={styles.container}>
      <FlatList
        data={CarouselItems}
        horizontal={true}
        renderItem={(item) => {
          return <RenderItem item={item.item} />
        }}
        keyExtractor={(item) => item.id}
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        bounced={false}
      />
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',
    height: scale(200),
    borderColor: 'lightgray',
    borderWidth: 2,
    marginTop: scale(20),
    borderRadius: scale(20),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },

})