import { StyleSheet, Text, View, Image, TouchableOpacity, Linking, Dimensions } from 'react-native'
import React from 'react'
import { scale } from '../../../common/common'

export const BannerItem = (props) => {

  const { bannerImg, imgBg, link, text, textBtn } = props.data
  const windowWidth = Dimensions.get('window').width

  return (
    <View style={[styles.container, { width: windowWidth }]}>

      <Image style={styles.bannerBg} source={imgBg} resizeMode='cover' />

      <View style={{ flexDirection: 'row' }}>

        <View>
          <Text style={styles.bannerText}>{text}</Text>
          <TouchableOpacity style={styles.bannerBtn} onPress={() => {
            Linking.openURL(link)
          }}
          >
            <Text style={styles.bannerBtnText}>{textBtn}</Text>
          </TouchableOpacity>
        </View>

        <Image style={styles.bannerLogo} source={bannerImg} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%'
  },
  bannerBg: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  },
  bannerText: {
    color: 'white',
    fontSize: scale(30),
    marginTop: scale(15),
    marginLeft: scale(15),
    textAlign: 'center'
  },
  bannerBtn: {
    borderColor: 'white',
    borderWidth: scale(2),
    borderRadius: scale(50),
    width: scale(220),
    height: scale(120),
    marginTop: scale(15),
    marginLeft: scale(50)
  },
  bannerBtnText: {
    color: 'white',
    alignSelf: 'center',
    marginTop: scale(30),
    fontSize: scale(35)
  },
  bannerLogo: {
    width: scale(400),
    height: scale(200),
    marginLeft: scale(20),
    marginTop: scale(20)
  }
})