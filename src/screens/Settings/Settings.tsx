import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native';
import { scale } from '../../common/common'
import { colors } from '../../common/colors'

export const Settings = () => {

    const navigation = useNavigation()

    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>Settings</Text>

            <TouchableOpacity
                style={styles.logoutBtn}
                onPress={() => {
                    navigation.replace('Login')
                }}>
                <Text style={styles.logoutBtnText}>Go to login page</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerText: {
        alignSelf: 'center',
        marginTop: scale(40),
        fontSize: scale(40)
    },
    logoutBtn: {
        position: 'absolute',
        alignSelf: 'center',
        width: scale(250),
        height: scale(66),
        borderRadius: scale(30),
        backgroundColor: colors.mainOrange,
        bottom: scale(180)
    },
    logoutBtnText: {
        color: 'white',
        fontSize: scale(30),
        alignSelf: 'center',
        marginTop: scale(10)
    }
})