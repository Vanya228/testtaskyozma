import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React, {useState, useEffect} from 'react'
import { scale } from '../../common/common'
import Icon from "react-native-vector-icons/MaterialIcons";
import { useNavigation } from '@react-navigation/native';
import { userSelector } from '../../store/selectors/login';
import { useSelector } from 'react-redux';

export const Profile = () => {

    const navigation = useNavigation()
    const userData = useSelector(userSelector)

    const [userName, setUserName] = useState('Unknown')
    const [userLocation, setUserLocation] = useState('Unknown')
    const [userAge, setUserAge] = useState('Unknown')

    useEffect(()=>{
        setUserName(userData.user.name)
        setUserLocation(userData.user.location)
        setUserAge(userData.user.age)
    }, [])

    return (
        <View>
            <View style={styles.headerWrapper}>
                <TouchableOpacity onPress={()=> {navigation.goBack()}}>
                    <Icon name="arrow-back-ios" size={24} style={styles.icon} />
                </TouchableOpacity>
                <Text style={styles.topText}>Profile</Text>
            </View>
            
            <View style={styles.userDataContainer}>
                <Text style={[styles.infoText, {marginTop: scale(100)}]}>Name: {userName}</Text>
                <Text style={styles.infoText}>Age: {userAge}</Text>
                <Text style={styles.infoText}>Location: {userLocation}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    topText: {
        fontSize: scale(40),
        alignSelf: 'center'
    },
    headerWrapper: {
        width: '58%',
        height: scale(130),
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    icon: {
        marginTop: scale(45),
        marginLeft: scale(40)
    },
    tempText: {
        alignSelf: 'center',
        fontSize: scale(40),
        color: 'gray',
        marginTop: scale(50)
    },
    userDataContainer: {
        width: '90%',
        alignSelf: 'center',
        height: scale(400),
        backgroundColor: 'white',
        borderRadius: scale(50)
    },
    infoText: {
        fontSize: scale(35),
        marginLeft: scale(30)
    }
})