export const userHighlights = {};

export const cards = [
  [
    {
      id: 1,
      iconName: "airplanemode-active",
      text: "Cat 1",
      image: require("../assets/temp/tempImages/cat1.jpeg"),
      action: "navToLogin",
    },
    {
      id: 2,
      iconName: "agriculture",
      text: "Cat 2",
      image: require("../assets/temp/tempImages/cat2.jpeg"),
      action: "navToSettings",
    },
    {
      id: 3,
      iconName: "airport-shuttle",
      text: "Cat 3",
      image: require("../assets/temp/tempImages/cat3.jpeg"),
      action: "navToLogin",
    },
    {
      id: 4,
      iconName: "android",
      text: "Cat 4",
      image: require("../assets/temp/tempImages/cat4.jpeg"),
      action: "navToSettings",
    },
    {
      id: 5,
      iconName: "airplanemode-active",
      text: "Cat 5",
      image: require("../assets/temp/tempImages/cat1.jpeg"),
      action: "navToLogin",
    },
    {
      id: 6,
      iconName: "agriculture",
      text: "Cat 6",
      image: require("../assets/temp/tempImages/cat2.jpeg"),
      action: "navToSettings",
    },
    {
      id: 7,
      iconName: "airport-shuttle",
      text: "Cat 7",
      image: require("../assets/temp/tempImages/cat3.jpeg"),
      action: "navToLogin",
    },
    {
      id: 8,
      iconName: "android",
      text: "Cat 8",
      image: require("../assets/temp/tempImages/cat4.jpeg"),
      action: "navToSettings",
    },
  ],
  [
    {
      id: 1,
      iconName: "airplanemode-active",
      text: "Cat 21",
      image: require("../assets/temp/tempImages/cat1.jpeg"),
      action: "navToLogin",
    },
    {
      id: 2,
      iconName: "agriculture",
      text: "Cat 22",
      image: require("../assets/temp/tempImages/cat2.jpeg"),
      action: "navToSettings",
    },
    {
      id: 3,
      iconName: "airport-shuttle",
      text: "Cat 33",
      image: require("../assets/temp/tempImages/cat3.jpeg"),
      action: "navToLogin",
    },
    {
      id: 4,
      iconName: "android",
      text: "Cat 44",
      image: require("../assets/temp/tempImages/cat4.jpeg"),
      action: "navToSettings",
    },
    {
      id: 5,
      iconName: "airplanemode-active",
      text: "Cat 55",
      image: require("../assets/temp/tempImages/cat1.jpeg"),
      action: "navToLogin",
    },
    {
      id: 6,
      iconName: "agriculture",
      text: "Cat 66",
      image: require("../assets/temp/tempImages/cat2.jpeg"),
      action: "navToSettings",
    },
    {
      id: 7,
      iconName: "airport-shuttle",
      text: "Cat 77",
      image: require("../assets/temp/tempImages/cat3.jpeg"),
      action: "navToLogin",
    },
    {
      id: 8,
      iconName: "android",
      text: "Cat 28",
      image: require("../assets/temp/tempImages/cat4.jpeg"),
      action: "navToSettings",
    },
  ],
  [
    {
      id: 1,
      iconName: "airplanemode-active",
      text: "Cat 441",
      image: require("../assets/temp/tempImages/cat1.jpeg"),
      action: "navToLogin",
    },
    {
      id: 2,
      iconName: "agriculture",
      text: "Cat 2234",
      image: require("../assets/temp/tempImages/cat2.jpeg"),
      action: "navToSettings",
    },
    {
      id: 3,
      iconName: "airport-shuttle",
      text: "Cat 3234",
      image: require("../assets/temp/tempImages/cat3.jpeg"),
      action: "navToLogin",
    },
    {
      id: 4,
      iconName: "android",
      text: "Cat 424",
      image: require("../assets/temp/tempImages/cat4.jpeg"),
      action: "navToSettings",
    },
    {
      id: 5,
      iconName: "airplanemode-active",
      text: "Cat 23411",
      image: require("../assets/temp/tempImages/cat1.jpeg"),
      action: "navToLogin",
    },
    {
      id: 6,
      iconName: "agriculture",
      text: "Cat 2234",
      image: require("../assets/temp/tempImages/cat2.jpeg"),
      action: "navToSettings",
    },
    {
      id: 7,
      iconName: "airport-shuttle",
      text: "Cat 35345345",
      image: require("../assets/temp/tempImages/cat3.jpeg"),
      action: "navToLogin",
    },
    {
      id: 8,
      iconName: "android",
      text: "Cat 42345",
      image: require("../assets/temp/tempImages/cat4.jpeg"),
      action: "navToSettings",
    },
  ]
];
export const bannerItems = [
  {
    id: 1,
    imgBg: require('../assets/temp/tempImages/purp-bg.jpeg'),
    text: "It's time to buy some\nfood for your friend!",
    link: 'https://www.google.com',
    textBtn: 'Purchase',
    bannerImg: require('../assets/temp/tempImages/cat.png')
  },
  {
    id: 2,
    imgBg: require('../assets/temp/tempImages/orange-bg.jpeg'),
    text: "Take a look for\nthe best videos ever!",
    link: 'https://www.youtube.com',
    textBtn: 'View',
    bannerImg: require('../assets/temp/tempImages/youtube.png')
  },
  {
    id: 3,
    imgBg: require('../assets/temp/tempImages/black-bg.jpeg'),
    text: "Just take a look\nat yours messages!",
    link: 'https://www.gmail.com',
    textBtn: 'Open',
    bannerImg: require('../assets/temp/tempImages/gmail.png')
  },
  {
    id: 4,
    imgBg: require('../assets/temp/tempImages/white-bg.jpeg'),
    text: "It's time to make some\ntweets for the world!",
    link: 'https://twitter.com',
    textBtn: 'Open',
    bannerImg: require('../assets/temp/tempImages/twitter.png')
  },
]

export const cardSwipeData = [
  {
    id: 1,
    img: require('../assets/temp/tempImages/cat2.jpeg'),
    text: 'Some text...Some text...Some text...'
  },
  {
    id: 2,
    img: require('../assets/temp/tempImages/cat3.jpeg'),
    text: 'Some text...Some text...Some text...'
  },
  {
    id: 3,
    img: require('../assets/temp/tempImages/cat2.jpeg'),
    text: ''
  },
]