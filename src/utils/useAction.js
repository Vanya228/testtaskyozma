import React, { useCallback } from "react";
import { useDispatch } from "react-redux";

export const useAction = (actionCreator) => {
  const dispatch = useDispatch();
  return useCallback(
    (arg) => dispatch(actionCreator(arg)),
    [dispatch, actionCreator]
  );
};
