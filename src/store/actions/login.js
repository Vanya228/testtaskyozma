import { createAction } from '@reduxjs/toolkit';

export const addUserDataAction = createAction('add user data action');