import { createReducer } from "@reduxjs/toolkit";
import { addUserDataAction } from "../actions/login";

const initialState = {
    user: {
        name: "",
        age: null,
        location: "",
    }
};

const reducer = createReducer(initialState, {
  [addUserDataAction.type]: (state, { payload }) => {
    console.log('payload', payload)
    console.log('state', state)
    return {
      ...state,
      user: payload,
    };
  },
});

export default reducer;
