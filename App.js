/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Ionic from "react-native-vector-icons/Ionicons";

import React from "react";

import { Login } from "./src/screens/Login/Login";
import { Home } from "./src/screens/Home/Home";
import { Profile } from "./src/screens/Profile/Profile";
import { Settings } from "./src/screens/Settings/Settings";
import store from "./store";
import { Provider as ReduxProvider } from "react-redux";

const { Navigator, Screen } = createStackNavigator();
const Tab = createBottomTabNavigator();

function HomeTabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, size, colour }) => {
          let iconName;
          if (route.name === "Home") {
            iconName = focused ? "home" : "home-outline";
            size = focused ? size + 8 : size + 5;
          } else if (route.name === "Settings") {
            iconName = focused ? "settings" : "settings-outline";
            size = focused ? size + 8 : size + 5;
          }
          return <Ionic name={iconName} size={size} colour={colour} />;
        },
      })}
      tabBarOption={{
        activeTintColor: "black",
        inactiveTintColor: "black",
        showLabel: false,
      }}
    >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Settings" component={Settings} />
    </Tab.Navigator>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <ReduxProvider store={store}>
        <Navigator screenOptions={{ headerShown: false }}>
          <Screen name="Login" component={Login} />
          <Screen name="Home" component={HomeTabs} />
          <Screen name="Profile" component={Profile} />
          <Screen name="Settings" component={Settings} />
        </Navigator>
      </ReduxProvider>
    </NavigationContainer>
  );
};

export default App;
