import { combineReducers, configureStore } from "@reduxjs/toolkit";
import login from './src/store/reducers/login'

const rootReducer = combineReducers({
    login
})

export type RootState = ReturnType<typeof rootReducer>

const store = configureStore({
    reducer: rootReducer,
})

export default store;